#!/bin/sh
scriptDir=/home/esendexservice/update_script
repoServer=reposerver
extraPuppetParams=$1

echo "1.  Get Hiera data..."
mv /etc/puppetlabs/puppet/hiera.yaml /etc/puppetlabs/puppet/hiera.yaml.old -f
mkdir /var/lib/hiera
/usr/bin/wget -nv --directory-prefix="/etc/puppetlabs/puppet" http://${repoServer}/puppet/hiera.yaml
/usr/bin/wget -nv -r --no-parent -nH --cut-dirs=2 -R "*index.html*" --directory-prefix="/var/lib/hiera/" http://${repoServer}/puppet/hieradata/


echo "2.  Remove all existing modules..."
rm /etc/puppetlabs/puppet/modules/* -rf

echo "3.  Downloading and installing puppet module packages in order of dependency..."
/usr/bin/wget -nc -nv -P ${scriptDir} "http://${repoServer}/mod-pkgs/esendex/esendex-profile-0.1.0.tar.gz"
/usr/bin/wget -nc -nv -P ${scriptDir} "http://${repoServer}/mod-pkgs/esendex/esendex-role-0.1.0.tar.gz"
/usr/bin/wget -nc -nv -P ${scriptDir} "http://${repoServer}/mod-pkgs/esendex/esendex-networking-0.1.0.tar.gz"
puppet module install ${scriptDir}/esendex-profile-0.1.0.tar.gz --ignore-dependencies --force 
puppet module install ${scriptDir}/esendex-role-0.1.0.tar.gz --ignore-dependencies --force 
puppet module install ${scriptDir}/esendex-networking-0.1.0.tar.gz --ignore-dependencies --force

echo "4.  Installing other 3rd party modules..."
mkdir ${scriptDir}/imported
echo "4a. Get them."
/usr/bin/wget -r -nv --no-parent -nH --cut-dirs=2 -R "*index.html*" -P ${scriptDir}/imported http://${repoServer}/mod-pkgs/imported/
echo "4b. Install them."
find ${scriptDir}/imported -iname "*.tar.gz" -type f -exec puppet module install --ignore-dependencies --force {} \;

echo "Copying nodes.pp manifest..."
rm /etc/puppetlabs/puppet/manifests/nodes.pp -f
/usr/bin/wget -nc -nv --directory-prefix=/etc/puppetlabs/puppet/manifests http://${repoServer}/puppet/manifests/nodes.pp

# Work out Environment
FQDN=`hostname -A | awk '{printf $1}'`
suffix=`echo ${FQDN} | sed 's/[^.]*//'`

case ${suffix} in
  ".dev.lab")
    environment=rnd
    ;;
  ".ops.lab")
    environment=staging
    ;;
  ".esendex.com")
    environment=pat
    ;;
  ".datacentre.esendex.com")
    environment=production
    ;;
  *)
    echo "Unable to determine Environment for ${FQDN}"
	exit 1
esac

echo "5.  Execute puppet commands"
puppet apply /etc/puppetlabs/puppet/manifests/nodes.pp --verbose --environment=${environment} --detailed-exitcodes ${extraPuppetParams}

#if [ "$?" != 0 ];
#  exit $?
#fi

EXIT=$?
case ${EXIT} in
  "0")
    echo "No Errors & No Changes"
    exit 0
    ;;
  "2")
    echo "No Errors but Changes Made"
    exit 0
    ;;
  "4")
    echo "!!!!!  ERROR EXECUTING PUPPET APPLY & NO CHANGES  !!!!!"
    exit ${EXIT}
    ;;
  "6")
    echo "!!!!!  ERROR EXECUTING PUPPET APPLY & CHANGES MADE  !!!!!"
    exit ${EXIT}
    ;;
  *)
    echo "Unable to determine Problem, EXIT CODE ${EXIT}"
	exit ${EXIT}
esac